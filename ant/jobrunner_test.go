package ant

import (
	"io/ioutil"
	"os"
	"testing"
)

func TestNewJobRunner(t *testing.T) {
	datadir, err := ioutil.TempDir("", "testing-data")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(datadir)
	spd, err := newSpd("spd", datadir, "localhost:31337", "localhost:31338", "localhost:31339", "")
	if err != nil {
		t.Fatal(err)
	}
	defer stopSpd("localhost:31337", spd.Process)

	j, err := newJobRunner("localhost:31337", "", datadir)
	if err != nil {
		t.Fatal(err)
	}
	defer j.Stop()
}
