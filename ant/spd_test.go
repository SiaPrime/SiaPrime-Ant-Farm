package ant

import (
	"io/ioutil"
	"os"
	"testing"

	"gitlab.com/SiaPrime/SiaPrime/node/api/client"
)

// TestNewSpd tests that NewSpd creates a reachable Sia API
func TestNewSpd(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}

	datadir, err := ioutil.TempDir("", "sia-testing")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(datadir)

	spd, err := newSpd("spd", datadir, "localhost:9990", "localhost:0", "localhost:0", "")
	if err != nil {
		t.Error(err)
		return
	}
	defer spd.Process.Kill()

	c := client.New("localhost:9990")
	if _, err := c.ConsensusGet(); err != nil {
		t.Error(err)
	}
	spd.Process.Kill()

	// verify that NewSpd returns an error given invalid args
	_, err = newSpd("spd", datadir, "this_is_an_invalid_addres:1000000", "localhost:0", "localhost:0", "")
	if err == nil {
		t.Fatal("expected newspd to return an error with invalid args")
	}
}
